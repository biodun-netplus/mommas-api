<?php
   
require APPPATH . 'libraries/REST_Controller.php';
     
class Payment extends REST_Controller {
    
	  /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function __construct() {
       parent::__construct();
       $this->load->database();
    }
       
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
	public function index_get($id = 0)
	{
        $payment_id = $this->get('payment_id');
        $meter_no = $this->get('meter_no');
        $merchant_code = $this->get('merchant_code');
        $amount = $this->get('amount');
        $server_ip = $this->get('server_ip');
         //$endpoint = "http://197.211.39.206:8080/MEMMCOLWebServices/webresources/PaymentV2/$MeterNo/prepaid/102/$payment_id/$Tokenamount";
        $url = "http://$server_ip/MEMMCOLWebServices/webresources/PaymentV2/$meter_no/prepaid/$merchant_code/$payment_id/$amount";
        $ch = curl_init($url);
           
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_POST, true);
        $result = curl_exec($ch);

       
        if($result == false)
        {    
            $error = json_encode([curl_errno($ch)]);
            $this->response($error, REST_Controller::HTTP_OK);
            return false;
        }

       
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch); 
        if ($httpcode == 200) {
           $tokenDecode = array(json_decode($result));   
           $this->response($tokenDecode, REST_Controller::HTTP_OK);
        }
        return false;
	}

    	
}