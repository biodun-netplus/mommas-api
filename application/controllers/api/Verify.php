<?php
   
require APPPATH . 'libraries/REST_Controller.php';
     
class Verify extends REST_Controller {
    
	  /**
     * Get All Data from this method.
     *
     * @return Response
    */
    public function __construct() {
       parent::__construct();
       $this->load->database();
    }
       
    /**
     * Get All Data from this method.
     *
     * @return Response
    */
	public function index_get($id = 0)
	{
        $server_ip = $this->get('server_ip');
        $meter_no = $this->get('meter_no');
        $merchant_code = $this->get('merchant_code');

        $url = "http://$server_ip/MEMMCOLWebServices/webresources/IdentificationV2/$merchant_code/$meter_no";
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);                     // send the request
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        //if Mommas change to xml implement this
        //$xml = simplexml_load_string($responsexml);
        //$response = json_encode($xml);
       
        curl_close($ch);
        if ($httpcode == 200) {
            $response = array(json_decode($response));
        
        //     var_dump(json_encode(json_decode($response)));
        //    var_dump(json_encode($response));
        //    var_dump("ORIGNIAL");
        //    var_dump(json_encode($response));
           // $response = json_decode($response);   
          
            $this->response($response, REST_Controller::HTTP_OK);
        }
        return false;
             
	}
    	
}